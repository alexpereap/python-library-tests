import requests
from bs4 import BeautifulSoup
page_url = 'http://laravel-simple-auth-example.alexpereap.com/'
login_url = 'http://laravel-simple-auth-example.alexpereap.com/login'

session_requests = requests.session()

r = session_requests.get(page_url)
print(r.status_code)

soup = BeautifulSoup(r.content, 'html.parser')
token = soup.find('input',{'name':'_token'}).get('value')

print(token)

payload = {

    "username" : "alexp",
    "password" : "Williams",
    "_token" : token
}

result_with_auth = session_requests.post(
	login_url, 
	data = payload, 
	headers = dict(referer=login_url)
)

print(result_with_auth.status_code)
# print(result_with_auth.content)

table_data = []

soup_auth = BeautifulSoup(result_with_auth.content, 'html.parser')
# print( soup_auth.prettify() )
table = soup_auth.find('table',class_='table')
table_body = table.find('tbody')
rows = table_body.find_all('tr')

for row in rows:
    cols = row.find_all('td')
    cols = [ele.text.strip() for ele in cols]
    table_data.append([ele for ele in cols if ele]) # Get rid of empty values

print(table_data)