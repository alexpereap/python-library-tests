from bs4 import BeautifulSoup
import requests
import os

path = os.path.dirname(os.path.abspath(__file__))
r = requests.get('https://www.theguardian.com/politics/2018/aug/19/brexit-tory-mps-warn-of-entryism-threat-from-leave-eu-supporters')

print(r.status_code)

soup = BeautifulSoup(r.content, 'html.parser')

article_paragraphs = soup.find(id="article").find(class_='content__article-body from-content-api js-article__body').find_all('p')
article_images = soup.find(id='article').find_all('img')

for x in article_images:
    print( x['src'] )

article_metatags = soup.find(id='article').find_all('meta')
print(article_metatags)

#page title
print( soup.title.string )

f = open(path + "/../site.txt", "w+")



for x in article_paragraphs:
    f.write( x.get_text() + '\n' )
    
f.close()